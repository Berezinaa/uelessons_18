﻿// Homework18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Player {
public:
    string name;
    int score;
};

bool comparePlayers(Player p1, Player p2) {
    return p1.score > p2.score;
}

int main() {
    int numPlayers;
    cout << "How many players do you want to add? ";
    cin >> numPlayers;

    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; i++) {
        cout << "Enter the name of player " << i + 1 << ": ";
        cin >> players[i].name;
        cout << "Enter the score of player " << i + 1 << ": ";
        cin >> players[i].score;
    }

    sort(players, players + numPlayers, comparePlayers);

    cout << "\nSorted list of players:\n";
    for (int i = 0; i < numPlayers; i++) {
        cout << players[i].name << " - " << players[i].score << " points\n";
    }

    delete[] players;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
